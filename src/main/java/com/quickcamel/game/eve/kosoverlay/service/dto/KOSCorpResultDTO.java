/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Description: CVA API's corp object
 *
 * @author Louis Burton
 */
@XmlRootElement
@XmlType(name = "corp")
public class KOSCorpResultDTO extends KOSResultDTO {

    private String m_ticker;
    private Boolean m_npc;
    private KOSAllianceResultDTO m_alliance;

    public String getTicker() {
        return m_ticker;
    }

    public void setTicker(String ticker) {
        m_ticker = ticker;
    }

    public Boolean getNpc() {
        return m_npc;
    }

    public void setNpc(Boolean npc) {
        m_npc = npc;
    }

    public KOSAllianceResultDTO getAlliance() {
        return m_alliance;
    }

    public void setAlliance(KOSAllianceResultDTO alliance) {
        m_alliance = alliance;
    }

    public String toString() {
        return new ToStringBuilder(this).
                appendSuper(super.toString()).
                append("ticker", m_ticker).
                append("npc", m_npc).
                append("alliance", m_alliance).
                toString();
    }
}
