package com.quickcamel.game.eve.kosoverlay.service.rest;

import java.io.IOException;
import java.io.InputStream;

/**
 * Cleans up JSON responses
 *
 * @author Louis Burton
 */
public class JSONCleaningStream extends InputStream {

    private final InputStream m_stream;
    private int m_lastChar;

    public JSONCleaningStream(InputStream stream) {
        m_stream = stream;
    }

    @Override
    public int read() throws IOException {
        int next;

        while (true) {
            next = m_stream.read();
            if (next != ',' || m_lastChar != '[') {
                if (next > 32) {
                    m_lastChar = next;
                }
                return next;
            }
        }
    }
}
