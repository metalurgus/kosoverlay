package com.quickcamel.game.eve.kosoverlay.service.rest;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;
import java.io.IOException;
import java.io.InputStream;

/**
 * Attempts to clean up badly formed JSON before marshalling attempts
 *
 * @author Louis Burton
 */
public class JSONCleaningReaderInterceptor implements ReaderInterceptor {

    @Override
    public Object aroundReadFrom(ReaderInterceptorContext context) throws IOException, WebApplicationException {
        final InputStream originalInputStream = context.getInputStream();
        context.setInputStream(new JSONCleaningStream(originalInputStream));
        return context.proceed();
    }
}
