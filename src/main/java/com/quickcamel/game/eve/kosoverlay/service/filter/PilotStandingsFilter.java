/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.filter;

import com.beimin.eveapi.response.eve.CharacterInfoResponse;
import com.google.common.cache.LoadingCache;
import com.quickcamel.game.eve.kosoverlay.service.user.ICurrentUserContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.*;

/**
 * Filters out pilots with a known standing (works concurrently for speed)
 *
 * @author Louis Burton
 */
public class PilotStandingsFilter implements IPilotFilter {

    private static final Logger m_logger = LoggerFactory.getLogger(PilotStandingsFilter.class);

    @Inject
    private ICurrentUserContext m_userContext;

    @Resource(name = "nameToIdCache")
    private LoadingCache<String, Long> m_nameToIdCache;

    @Resource(name = "idToCharacterInfoCache")
    private LoadingCache<Long, CharacterInfoResponse> m_idToCharacterInfoCache;

    @Resource(name = "pilotStandingsFilterExecutorService")
    private ExecutorService m_executorService;

    @Override
    public Set<String> getFilteredPilots(Collection<String> pilotNames) {
        Set<String> result = new HashSet<>();
        Map<String, Future<Boolean>> futures = new HashMap<>();
        for (String pilotName : pilotNames) {
            if (m_userContext.getPilotName() != null && m_userContext.getPilotName().equals(pilotName)) {
                m_logger.debug("Filtering out yourself: '" + pilotName + "'");
            }
            else {
                futures.put(pilotName, m_executorService.submit(new PilotStandingsCallable(pilotName)));
            }
        }

        for (Map.Entry<String, Future<Boolean>> future : futures.entrySet()) {
            try {
                if (future.getValue().get()) {
                    result.add(future.getKey());
                }
            }
            catch (InterruptedException | ExecutionException e) {
                m_logger.debug("Unable to get Id for standings filter on '" + future.getKey() + "', will include in check for safety.", e);
                result.add(future.getKey());
            }
        }
        return result;
    }

    private final class PilotStandingsCallable implements Callable<Boolean> {

        private String m_pilotName;

        private PilotStandingsCallable(String pilotName) {
            m_pilotName = pilotName;
        }

        @Override
        public Boolean call() throws Exception {
            Long pilotId = m_nameToIdCache.get(m_pilotName);
            CharacterInfoResponse response = m_idToCharacterInfoCache.get(pilotId);
            boolean hasPilotStanding = m_userContext.getStanding(pilotId) != null;
            boolean hasCorpStanding = response.getCorporationID() > 0 && m_userContext.getStanding(response.getCorporationID()) != null;
            boolean hasAllianceStanding = response.getAllianceID() != null && response.getAllianceID() > 0 && m_userContext.getStanding(response.getAllianceID()) != null;
            boolean sameCorp = m_userContext.getCorporationId() != null && response.getCorporationID() != 0 && m_userContext.getCorporationId().equals(response.getCorporationID());
            boolean sameAlliance = m_userContext.getAllianceId() != null && m_userContext.getAllianceId() > 0 && response.getAllianceID() != null && response.getAllianceID() > 0 && m_userContext.getAllianceId().equals(response.getAllianceID());
            if (!sameCorp && !sameAlliance && !hasAllianceStanding && !hasCorpStanding && !hasPilotStanding) {
                return true;
            }
            else {
                m_logger.debug("Filtering out pilot with name '" + m_pilotName + "' : hasPilotStanding=" + hasPilotStanding + ", hasCorpStanding=" + hasCorpStanding + ", hasAllianceStanding=" + hasAllianceStanding + ", sameCorp=" + sameCorp + ", sameAlliance=" + sameAlliance);
                return false;
            }
        }
    }
}
