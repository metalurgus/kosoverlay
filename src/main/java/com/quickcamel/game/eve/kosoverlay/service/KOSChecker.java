/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service;

import com.beimin.eveapi.model.eve.CharacterEmployment;
import com.beimin.eveapi.response.corporation.CorpSheetResponse;
import com.beimin.eveapi.response.eve.CharacterInfoResponse;
import com.google.common.cache.LoadingCache;
import com.quickcamel.game.eve.kosoverlay.configuration.ConfigKeyConstants;
import com.quickcamel.game.eve.kosoverlay.configuration.IConfigManager;
import com.quickcamel.game.eve.kosoverlay.service.dto.*;
import com.quickcamel.game.eve.kosoverlay.service.user.ICurrentUserContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * This contains the main KOS logic for this tool. It is built off of the CVA KOS API, and the EVE API.
 *
 * @author Louis Burton
 */
public class KOSChecker implements IKOSChecker {

    private static final Logger m_logger = LoggerFactory.getLogger(KOSChecker.class);
    private static final String STANDINGS_SHORT_SUMMARY = "(S)";

    @Inject
    private IConfigManager m_configManager;

    @Inject
    private IKOSSummaryHelper m_kosSummaryHelper;

    @Resource(name = "pilotCVACache")
    private LoadingCache<String, KOSResultContainerDTO> m_pilotCVACache;

    @Resource(name = "corpCVACache")
    private LoadingCache<String, KOSResultContainerDTO> m_corpCVACache;

    @Resource(name = "allianceCVACache")
    private LoadingCache<String, KOSResultContainerDTO> m_allianceCVACache;

    @Resource(name = "nameToIdCache")
    private LoadingCache<String, Long> m_nameToIdCache;

    @Resource(name = "idToCharacterInfoCache")
    private LoadingCache<Long, CharacterInfoResponse> m_idToCharacterInfoCache;

    @Resource(name = "idToNameCache")
    private LoadingCache<Long, String> m_idToNameCache;

    @Resource(name = "corpIdToCorpSheetCache")
    private LoadingCache<Long, CorpSheetResponse> m_corpIdToCorpSheetCache;

    @Inject
    private ICurrentUserContext m_userContext;

    @Override
    public KOSResult checkPilot(String pilotName) {
        KOSResult result = new KOSResult();
        result.setPilotName(pilotName);
        KOSStatusSummary summary = new KOSStatusSummary();
        try {
            // Build details
            KOSPilotResultDTO pilotDetails = getPilotInformation(pilotName);

            if (pilotDetails != null) {
                result.setPilotDetails(pilotDetails);
                m_kosSummaryHelper.updateSummaryFromPilot(pilotDetails, summary);

                // If in an NPC corporation and not identified as KOS, check last corp and last alliance if configured.
                if (pilotDetails.getCorp() != null &&
                        (pilotDetails.getCorp().getNpc() != null && pilotDetails.getCorp().getNpc()) &&
                        summary.getResolvedStatus() == EntityStatus.NEUTRAL
                        && Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.KOSCHECKER_CHECKLASTCORP, ConfigKeyConstants.KOSCHECKER_CHECKLASTCORP_DEFAULT))) {
                    checkLastCorpAndLastAlliance(pilotDetails, summary);
                }

                // TODO - When militia enlistment/decorations are exposed to the public API - check this too for Minmitar militia (http://wiki.eve-id.net/API_Wanted_Features#Public_Character_Information_.5Bpartly_implemented.5D)
                // Same Corp or Alliance
                if (summary.getResolvedStatus() == EntityStatus.NEUTRAL) {
                    checkSamePilotCorpAlliance(pilotDetails, summary);
                }
                // Standings check
                if (summary.getResolvedStatus() == EntityStatus.NEUTRAL) {
                    checkStandings(pilotDetails, summary);
                }
            }
        }
        catch (ExecutionException e) {
            // If any CVA API check results in an API error (not just 'No Results') - we can't trust the rest of the check and must error it completely
            m_logger.error("Erroring check for : " + pilotName, e);
            summary.setOverrideStatus(EntityStatus.ERROR);
        }
        result.setStatusSummary(summary);
        if (m_logger.isDebugEnabled()) {
            m_logger.debug("Result : " + result);
        }
        return result;
    }

    private KOSPilotResultDTO getPilotInformation(String pilotName) throws ExecutionException {
        // Look for Pilot at CVA
        KOSPilotResultDTO pilotInfo = getPilotFromCVA(pilotName);

        if (pilotInfo == null) {
            // No Pilot CVA results
            // If not found in CVA, get the key pilot information via the EVE API
            CharacterInfoResponse response = null;
            try {
                Long characterId = m_nameToIdCache.get(pilotName);
                response = m_idToCharacterInfoCache.get(characterId);
            }
            catch (ExecutionException e) {
                // The pilot does not exist to EVE - or there is some critical EVE API error
                m_logger.warn("Unable to load Pilot data from EVE API for " + pilotName, e);
            }

            if (response != null) {
                // Build basic pilot info from EVE API
                pilotInfo = buildPilotDTOFromEVEResponse(response);
                if (!StringUtils.isEmpty(response.getCorporation())) {
                    // Try CVA again with corporation
                    KOSCorpResultDTO corpInfo = getCorpFromCVA(response.getCorporation());

                    if (corpInfo == null) {
                        // No Pilot AND No Corp CVA results - Issue #9
                        // Build basic corporation info from EVE API
                        corpInfo = buildCorpDTOFromEVEResponse(response);
                        if (!StringUtils.isEmpty(response.getAlliance())) {
                            // Try CVA again with alliance
                            KOSAllianceResultDTO allianceInfo = getAllianceFromCVA(response.getAlliance());

                            if (allianceInfo == null) {
                                // No Pilot AND No Corp AND No Alliance CVA results
                                // Build basic alliance info from EVE API - do no more
                                allianceInfo = buildAllianceDTOFromEVEResponse(response);
                            }
                            corpInfo.setAlliance(allianceInfo);
                        }
                    }
                    pilotInfo.setCorp(corpInfo);
                }
            }
        }
        return pilotInfo;
    }

    private KOSPilotResultDTO getPilotFromCVA(String pilotName) throws ExecutionException {
        KOSResultContainerDTO result = m_pilotCVACache.get(pilotName);
        KOSPilotResultDTO pilotResult;
        // Errors should throw exception - no valid results is translated to null
        if (result.getTotal() <= 0
                || result.getResults() == null
                || result.getResults().size() == 0
                || !result.getResults().get(0).getType().equals("pilot")
                || !result.getResults().get(0).getLabel().equalsIgnoreCase(pilotName)) {
            pilotResult = null;
        }
        else {
            pilotResult = (KOSPilotResultDTO) result.getResults().get(0);
        }
        return pilotResult;
    }

    private KOSCorpResultDTO getCorpFromCVA(String corpName) throws ExecutionException {
        KOSResultContainerDTO result = m_corpCVACache.get(corpName);
        KOSCorpResultDTO corpResult;
        // Errors should throw exception - no valid results is translated to null
        if (result.getTotal() <= 0
                || result.getResults() == null
                || result.getResults().size() == 0
                || !result.getResults().get(0).getType().equals("corp")
                || !result.getResults().get(0).getLabel().equalsIgnoreCase(corpName)) {
            corpResult = null;
        }
        else {
            corpResult = (KOSCorpResultDTO) result.getResults().get(0);
        }
        return corpResult;
    }

    private KOSAllianceResultDTO getAllianceFromCVA(String alliance) throws ExecutionException {
        KOSResultContainerDTO result = m_allianceCVACache.get(alliance);
        KOSAllianceResultDTO allianceResult;
        // Errors should throw exception - no valid results is translated to null
        if (result.getTotal() <= 0
                || result.getResults() == null
                || result.getResults().size() == 0
                || !result.getResults().get(0).getType().equals("alliance")
                || !result.getResults().get(0).getLabel().equalsIgnoreCase(alliance)) {
            allianceResult = null;
        }
        else {
            allianceResult = (KOSAllianceResultDTO) result.getResults().get(0);
        }
        return allianceResult;
    }

    private KOSPilotResultDTO buildPilotDTOFromEVEResponse(CharacterInfoResponse response) {
        KOSPilotResultDTO pilotInfo = new KOSPilotResultDTO();
        pilotInfo.setIcon("error.png");
        pilotInfo.setEveId(response.getCharacterID());
        pilotInfo.setLabel(response.getCharacterName());
        pilotInfo.setKos(false);
        pilotInfo.setType("pilot");
        return pilotInfo;
    }

    private KOSCorpResultDTO buildCorpDTOFromEVEResponse(CharacterInfoResponse response) {
        // If this occurs for NPC corps, consider checking the EVE API to determine if an NPC corp
        KOSCorpResultDTO corpInfo = new KOSCorpResultDTO();
        corpInfo.setIcon("error.png");
        corpInfo.setLabel(response.getCorporation());
        corpInfo.setEveId(response.getCorporationID());
        if (response.getCorporationID() != 0L && isNPCCorpId(response.getCorporationID())) {
            corpInfo.setNpc(true);
        }
        corpInfo.setKos(false);
        corpInfo.setType("corp");
        return corpInfo;
    }

    private KOSAllianceResultDTO buildAllianceDTOFromEVEResponse(CharacterInfoResponse response) {
        KOSAllianceResultDTO allianceInfo = new KOSAllianceResultDTO();
        allianceInfo.setIcon("error.png");
        allianceInfo.setLabel(response.getAlliance());
        allianceInfo.setEveId(response.getAllianceID());
        allianceInfo.setKos(false);
        allianceInfo.setType("alliance");
        return allianceInfo;
    }

    private void checkLastCorpAndLastAlliance(KOSPilotResultDTO pilot, KOSStatusSummary kosStatusSummary) {
        try {
            // Get employment history
            CharacterInfoResponse infoResponse = m_idToCharacterInfoCache.get(pilot.getEveId());
            boolean foundNonNPC = false;
            Iterator<CharacterEmployment> iterator = infoResponse.getEmploymentHistory().iterator();
            while (iterator.hasNext() && !foundNonNPC) {
                CharacterEmployment employment = iterator.next();
                // Go through each employment and check for a non-NPC corp.
                // Only check if not their current corp and not a known NPC corporation (see com/quickcamel/game/eve/kosoverlay/service/npc-corporations.txt).
                if (employment.getCorporationID() != null &&
                        !pilot.getCorp().getEveId().equals(employment.getCorporationID()) &&
                        !isNPCCorpId(employment.getCorporationID())) {
                    // CVA API has to search by name - get corp name for Id
                    String corpName = m_idToNameCache.get(employment.getCorporationID());

                    KOSCorpResultDTO corpInfo = getCorpFromCVA(corpName);

                    if (corpInfo != null) {
                        if (!corpInfo.getNpc()) {
                            // Found a player corp! - this'll finish one way or another
                            foundNonNPC = true;
                            m_kosSummaryHelper.updateSummaryFromCorp(corpInfo, kosStatusSummary, true);
                        }
                    }
                    else {
                        // Just because the CVA KOS Database doesn't find this corp, doesn't mean it's not a friendly player corp
                        // We must give the benefit of the doubt that this isn't an NPC corp
                        foundNonNPC = true;
                        // Get Corp Info from EVE
                        CorpSheetResponse corpSheetResponse = m_corpIdToCorpSheetCache.get(employment.getCorporationID());
                        if (!StringUtils.isEmpty(corpSheetResponse.getAllianceName())) {
                            // Try CVA again with alliance
                            KOSAllianceResultDTO allianceInfo = getAllianceFromCVA(corpSheetResponse.getAllianceName());
                            if (allianceInfo != null) {
                                m_kosSummaryHelper.updateSummaryFromAlliance(allianceInfo, kosStatusSummary, true);
                            }
                        }
                    }
                }
            }
        }
        catch (ExecutionException e) {
            m_logger.error("Unable to perform last corp check : " + pilot, e);
            kosStatusSummary.setWarning(true);
        }
    }

    private void checkSamePilotCorpAlliance(KOSPilotResultDTO pilot, KOSStatusSummary kosStatusSummary) {
        String shortReason = null;
        if (pilot.getLabel().equals(m_userContext.getPilotName())) {
            kosStatusSummary.setPilotStatus(EntityStatus.BLUE_SAME_PILOT_OR_CORP);
            kosStatusSummary.setCorpStatus(EntityStatus.BLUE_SAME_PILOT_OR_CORP);
            kosStatusSummary.setAllianceStatus(EntityStatus.BLUE_SAME_ALLIANCE);
            shortReason = STANDINGS_SHORT_SUMMARY;
        }
        else if (pilot.getCorp() != null) {
            if (pilot.getCorp().getEveId().equals(m_userContext.getCorporationId())) {
                kosStatusSummary.setPilotStatus(EntityStatus.BLUE_10);
                kosStatusSummary.setCorpStatus(EntityStatus.BLUE_SAME_PILOT_OR_CORP);
                kosStatusSummary.setAllianceStatus(EntityStatus.BLUE_SAME_ALLIANCE);
                shortReason = STANDINGS_SHORT_SUMMARY;
            }
            else if (pilot.getCorp().getAlliance() != null
                    && pilot.getCorp().getAlliance().getEveId().equals(m_userContext.getAllianceId())) {
                kosStatusSummary.setPilotStatus(EntityStatus.BLUE_10);
                kosStatusSummary.setCorpStatus(EntityStatus.BLUE_10);
                kosStatusSummary.setAllianceStatus(EntityStatus.BLUE_SAME_ALLIANCE);
                shortReason = STANDINGS_SHORT_SUMMARY;
            }
        }
        if (m_logger.isDebugEnabled() && shortReason != null) {
            m_logger.debug("Result from Same Pilot/Corp/Alliance");
        }
        if (kosStatusSummary.getShortReason() == null) {
            kosStatusSummary.setShortReason(shortReason);
        }
    }

    private void checkStandings(KOSPilotResultDTO pilot, KOSStatusSummary kosStatusSummary) {
        String shortReason = null;
        EntityStatus pilotStanding = m_userContext.getStanding(pilot.getEveId());
        if (pilotStanding != null) {
            kosStatusSummary.setPilotStatus(pilotStanding);
            shortReason = STANDINGS_SHORT_SUMMARY;
        }
        if (pilot.getCorp() != null) {
            EntityStatus corpStanding = m_userContext.getStanding(pilot.getCorp().getEveId());
            if (corpStanding != null) {
                kosStatusSummary.setCorpStatus(corpStanding);
                shortReason = STANDINGS_SHORT_SUMMARY;
            }
            if (pilot.getCorp().getAlliance() != null) {
                EntityStatus allianceStanding = m_userContext.getStanding(pilot.getCorp().getAlliance().getEveId());
                if (allianceStanding != null) {
                    kosStatusSummary.setAllianceStatus(allianceStanding);
                    shortReason = STANDINGS_SHORT_SUMMARY;
                }
            }
        }
        if (kosStatusSummary.getShortReason() == null) {
            kosStatusSummary.setShortReason(shortReason);
        }
        if (m_logger.isDebugEnabled() && shortReason != null) {
            m_logger.debug("Result from Standings");
        }
    }

    private boolean isNPCCorpId(long corpId) {
        return corpId >= 1000002 && corpId <= 1000182;
    }
}