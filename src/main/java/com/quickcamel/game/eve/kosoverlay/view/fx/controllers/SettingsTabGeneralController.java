/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view.fx.controllers;

import com.quickcamel.game.eve.kosoverlay.configuration.ConfigKeyConstants;
import com.quickcamel.game.eve.kosoverlay.configuration.IConfigChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * Controller functionality for the settings tab - orientated around general considerations
 *
 * @author Louis Burton
 */
public class SettingsTabGeneralController extends SettingsTabController {

    private static final Logger m_logger = LoggerFactory.getLogger(SettingsTabGeneralController.class);

    public static final String MAXIMUM_DISPLAY_TOOLTIP = "The limit for the number of results that can be shown.";
    public static final String LARGE_BATCH_THRESHOLD_TOOLTIP = "The threshold for when to use large batch handling.\n" +
            "Must be lower than the Maximum Display, as Small Batches display all checks from the outset.";
    public static final String BRING_TO_FRONT_TOOLTIP = "Keeping a window on top can depend on your environment and other applications.\n" +
            "Turn this on if you need the KOS Overlay to aggressively bring itself to the front periodically.";
    public static final String FADE_TIMEOUT_TOOLTIP = "The amount of seconds waited once a check, or batch of checks, completes before fading out the results.";
    public static final String CUSTOM_SHORTCUT_TOOLTIP = "Initiates a KOS check on the clipboard contents via an alternative key.";
    public static final String BATCH_PRIMING_TOOLTIP = "Enables Large Batches to KOS check all necessary pilots at once for overall speed gains.\n" +
            "Uses the CVA API's 'multi' mode. Not beneficial when not wanting to KOS check all that are selected (i.e. 100s).";
    public static final String KOS_CHECK_ALL_TOOLTIP = "Continue to KOS Check all pilots in a large batch, even after no more can be displayed.\n" +
            "Useful when a full count of KOS pilots is needed in more populated systems, but results in more work.";
    public static final String CVA_THROTTLE_TOOLTIP = "Ensure only one connection to CVA is made at a time.\n" +
            "Requires restart to take effect.";

    @FXML
    private TextField m_largeBatchThreshold;

    @FXML
    private TextField m_resultsMaximum;

    @FXML
    private CheckBox m_bringToFront;

    @FXML
    private TextField m_kosKeyDisplay;

    @FXML
    private ToggleButton m_kosKeySet;

    @FXML
    private Button m_kosKeyClear;

    @FXML
    private CheckBox m_batchPriming;

    @FXML
    private TextField m_fadeTimeout;

    @FXML
    private CheckBox m_kosCheckAll;

    @FXML
    private CheckBox m_cvaThrottle;

    @Resource(name = "kosBatchService")
    private IConfigChangeListener m_batchServiceConfigListener;

    @Resource(name = "alwaysOnTopDialogue")
    private IConfigChangeListener m_alwaysOnTopDialogue;

    @Resource(name = "keyListener")
    private IConfigChangeListener m_keyListener;

    private int m_customKey = 0;
    private int m_customKeyModifiers = 0;

    @FXML
    public void initialize() {
        m_logger.debug("Initialising");
        m_largeBatchThreshold.setText(m_configManager.getValue(ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_THRESHOLD, ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_THRESHOLD_DEFAULT));
        m_resultsMaximum.setText(m_configManager.getValue(ConfigKeyConstants.RESULTS_MAXIMUM, ConfigKeyConstants.RESULTS_MAXIMUM_DEFAULT));
        m_fadeTimeout.setText(String.valueOf(Double.valueOf(m_configManager.getValue(ConfigKeyConstants.FADE_PAUSE_DURATION, ConfigKeyConstants.FADE_PAUSE_DURATION_DEFAULT)) / 1000));
        m_bringToFront.setSelected(Integer.parseInt(m_configManager.getValue(ConfigKeyConstants.OVERLAY_ALWAYSONTOP_DELAY_MS, ConfigKeyConstants.OVERLAY_ALWAYSONTOP_DELAY_MS_DEFAULT)) > 0);
        if (!StringUtils.isEmpty(m_configManager.getValue(ConfigKeyConstants.KEYBOARD_CUSTOM_KEY))) {
            m_customKey = Integer.valueOf(m_configManager.getValue(ConfigKeyConstants.KEYBOARD_CUSTOM_KEY));
        }
        else {
            m_customKey = 0;
        }
        if (!StringUtils.isEmpty(m_configManager.getValue(ConfigKeyConstants.KEYBOARD_CUSTOM_MODIFIERS))) {
            m_customKeyModifiers = Integer.valueOf(m_configManager.getValue(ConfigKeyConstants.KEYBOARD_CUSTOM_MODIFIERS));
        }
        else {
            m_customKeyModifiers = 0;
        }
        m_batchPriming.setSelected(Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.BATCHSERVICE_BATCHPRIMING, ConfigKeyConstants.BATCHSERVICE_BATCHPRIMING_DEFAULT)));
        m_kosKeyDisplay.setText(getKeyDisplayString(m_customKey, m_customKeyModifiers));
        m_kosCheckAll.setSelected(Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_KOSCHECKALL, ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_KOSCHECKALL_DEFAULT)));
        m_cvaThrottle.setSelected(Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.CVACHECKS_THROTTLED, ConfigKeyConstants.CVACHECKS_THROTTLED_DEFAULT)));
    }

    public void setKOSKey() {
        GlobalScreen.addNativeKeyListener(new ConfigKeyListener());
        m_kosKeyDisplay.setDisable(false);
    }

    public void clearKOSKey() {
        m_customKey = 0;
        m_customKeyModifiers = 0;
        m_kosKeyDisplay.setText("");
        m_kosKeyDisplay.setDisable(true);
    }

    public String validate() {
        String errorText = null;
        int resultsMax = Integer.parseInt(m_configManager.getValue(ConfigKeyConstants.RESULTS_MAXIMUM, ConfigKeyConstants.RESULTS_MAXIMUM_DEFAULT));
        int largeBatchThreshold = Integer.parseInt(m_configManager.getValue(ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_THRESHOLD, ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_THRESHOLD_DEFAULT));
        Double.parseDouble(m_fadeTimeout.getText());
        if (m_resultsMaximum.getText() != null && m_resultsMaximum.getText().trim().length() > 0) {
            resultsMax = Integer.parseInt(m_resultsMaximum.getText().trim());
        }
        if (m_largeBatchThreshold.getText() != null && m_largeBatchThreshold.getText().trim().length() > 0) {
            largeBatchThreshold = Integer.parseInt(m_largeBatchThreshold.getText().trim());
        }
        if (largeBatchThreshold > resultsMax) {
            errorText = "The Large Batch Threshold can't be higher than the Maximum Display. Small Batches display checks from the outset.";
        }
        return errorText;
    }

    public void save() {
        m_logger.debug("Saving settings");
        boolean needsBatchServiceUpdate = updateValueIfSet(ConfigKeyConstants.RESULTS_MAXIMUM, m_resultsMaximum.getText());
        needsBatchServiceUpdate = (updateValueIfSet(ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_THRESHOLD, m_largeBatchThreshold.getText()) || needsBatchServiceUpdate);
        needsBatchServiceUpdate = (updateValueIfSet(ConfigKeyConstants.FADE_PAUSE_DURATION, String.valueOf((long) (Double.valueOf(m_fadeTimeout.getText()) * 1000))) || needsBatchServiceUpdate);

        boolean needsAlwaysOnTopUpdate;
        if (!m_bringToFront.isSelected()) {
            needsAlwaysOnTopUpdate = updateValueIfSet(ConfigKeyConstants.OVERLAY_ALWAYSONTOP_DELAY_MS, "-1");
        }
        else {
            String existingAlwaysOnTopString = m_configManager.getValue(ConfigKeyConstants.OVERLAY_ALWAYSONTOP_DELAY_MS);
            if (existingAlwaysOnTopString == null || Integer.parseInt(existingAlwaysOnTopString) <= 0) {
                existingAlwaysOnTopString = "5000";
            }
            needsAlwaysOnTopUpdate = updateValueIfSet(ConfigKeyConstants.OVERLAY_ALWAYSONTOP_DELAY_MS, existingAlwaysOnTopString);
        }
        boolean needsKeyListenerUpdate = updateValueIfChanged(ConfigKeyConstants.KEYBOARD_CUSTOM_KEY, String.valueOf(m_customKey));
        needsKeyListenerUpdate = (updateValueIfSet(ConfigKeyConstants.KEYBOARD_CUSTOM_MODIFIERS, String.valueOf(m_customKeyModifiers)) || needsKeyListenerUpdate);

        updateValueIfSet(ConfigKeyConstants.BATCHSERVICE_BATCHPRIMING, String.valueOf(m_batchPriming.isSelected()));
        updateValueIfSet(ConfigKeyConstants.BATCHSERVICE_LARGEBATCH_KOSCHECKALL, String.valueOf(m_kosCheckAll.isSelected()));
        updateValueIfSet(ConfigKeyConstants.CVACHECKS_THROTTLED, String.valueOf(m_cvaThrottle.isSelected()));

        initialize();
        // TODO - clever auto registration of listeners to properties would be nice
        if (needsBatchServiceUpdate) {
            m_batchServiceConfigListener.notifyConfigChange();
        }
        if (needsAlwaysOnTopUpdate) {
            m_alwaysOnTopDialogue.notifyConfigChange();
        }
        if (needsKeyListenerUpdate) {
            m_keyListener.notifyConfigChange();
        }
    }

    class ConfigKeyListener implements NativeKeyListener {

        @Override
        public void nativeKeyPressed(NativeKeyEvent e) {
            m_customKey = e.getKeyCode();
            m_customKeyModifiers = e.getModifiers();
            if (e.getKeyCode() == 0) {
                m_kosKeyDisplay.setText("Key not supported");
            }
            else {
                m_kosKeyDisplay.setText(getKeyDisplayString(e.getKeyCode(), e.getModifiers()));
            }
        }

        @Override
        public void nativeKeyReleased(NativeKeyEvent e) {
            GlobalScreen.removeNativeKeyListener(this);
            m_kosKeyDisplay.setDisable(true);
            m_kosKeySet.setSelected(false);
        }

        @Override
        public void nativeKeyTyped(NativeKeyEvent e) {
        }
    }

    public String getKeyDisplayString(int keyCode, int keyModifiers) {
        String result;
        if (keyCode == 0) {
            result = "";
        }
        else {
            String keyCodeString = NativeKeyEvent.getKeyText(keyCode).replaceAll("Control", "Ctrl");
            StringBuilder fullKeyString = new StringBuilder();
            if (keyModifiers > 0) {
                fullKeyString.append(NativeKeyEvent.getModifiersText(keyModifiers));
                String[] keyCodeSplit = keyCodeString.split(" ");
                if (!ArrayUtils.contains(NativeKeyEvent.getModifiersText(keyModifiers).split("[+]"), keyCodeSplit[keyCodeSplit.length - 1])) {
                    fullKeyString.append("+");
                    fullKeyString.append(keyCodeString);
                }
            }
            else {
                fullKeyString.append(keyCodeString);
            }
            result = fullKeyString.toString();
        }
        return result;
    }
}