/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.user;

import com.beimin.eveapi.exception.ApiException;
import com.beimin.eveapi.parser.ApiAuthorization;
import com.beimin.eveapi.parser.eve.CharacterInfoParser;
import com.beimin.eveapi.parser.eve.CharacterLookupParser;
import com.beimin.eveapi.response.eve.CharacterInfoResponse;
import com.beimin.eveapi.response.eve.CharacterLookupResponse;
import com.quickcamel.game.eve.kosoverlay.configuration.*;
import com.quickcamel.game.eve.kosoverlay.service.EntityStatus;
import com.quickcamel.game.eve.kosoverlay.service.dto.KOSPilotResultDTO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Description: Holds information relevant to the current pilot using the KOS Overlay, using the Eve API to get information
 *
 * @author Louis Burton
 */
public class CurrentUserContext implements ICurrentUserContext, IConfigChangeListener {

    private static final Logger m_logger = LoggerFactory.getLogger(CurrentUserContext.class);

    @Inject
    private IConfigManager m_configManager;

    private String m_characterName;
    private Long m_corporationId;
    private Long m_allianceId;
    private AtomicBoolean m_standingsRefreshRunning = new AtomicBoolean(false);
    protected Timer m_standingsTimer = new Timer("StandingsTimer");
    protected Map<Long, Double> m_standings = null;

    @PostConstruct
    public void start() throws ApiException {
        reset();
        m_characterName = m_configManager.getValue(ConfigKeyConstants.API_CHARACTERNAME);
        if (StringUtils.isEmpty(m_characterName)) {
            if (m_logger.isDebugEnabled()) {
                m_logger.debug("No character name configured - will continue no further");
            }
        }
        else {
            if (m_logger.isDebugEnabled()) {
                m_logger.debug("CharacterName : " + m_characterName);
            }
            String characterIdStr = m_configManager.getValue(ConfigKeyConstants.API_CHARACTERID_PREFIX + m_characterName);
            long characterId;
            if (characterIdStr != null) {
                characterId = Long.valueOf(characterIdStr);
            }
            else {
                CharacterLookupParser lookupParser = CharacterLookupParser.getName2IdInstance();
                CharacterLookupResponse response = lookupParser.getResponse(m_characterName);
                characterId = response.getAll().iterator().next().getCharacterID();
                if (m_logger.isDebugEnabled()) {
                    m_logger.debug("CharacterId " + characterIdStr + " retrieved for " + m_characterName);
                }
                m_configManager.setValue(ConfigKeyConstants.API_CHARACTERID_PREFIX + m_characterName, String.valueOf(characterId));
            }
            if (m_corporationId == null && m_allianceId == null) {
                CharacterInfoResponse response = new CharacterInfoParser().getResponse(characterId);
                m_corporationId = response.getCorporationID();
                m_allianceId = response.getAllianceID();
                if (m_logger.isDebugEnabled()) {
                    m_logger.debug("CorporationId " + m_corporationId + ", AllianceId " + m_allianceId);
                }
            }
            String keyIdString = m_configManager.getValue(ConfigKeyConstants.API_KEYID);
            String vcode = m_configManager.getValue(ConfigKeyConstants.API_VCODE);
            boolean useAlliance = Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.STANDINGS_USEALLIANCE, ConfigKeyConstants.STANDINGS_USEALLIANCE_DEFAULT));
            boolean useCorp = Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.STANDINGS_USECORP, ConfigKeyConstants.STANDINGS_USECORP_DEFAULT));
            boolean usePersonal = Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.STANDINGS_USEPERSONAL, ConfigKeyConstants.STANDINGS_USEPERSONAL_DEFAULT));
            if (!StringUtils.isEmpty(keyIdString) && !StringUtils.isEmpty(vcode)) {
                int keyId = Integer.valueOf(keyIdString);
                ApiAuthorization apiAuthorization = new ApiAuthorization(keyId, characterId, vcode);
                if (m_standingsRefreshRunning.getAndSet(false)) {
                    m_standingsTimer.cancel();
                    m_standingsTimer = new Timer("StandingsTimer");
                }
                new Thread(new StandingsTask(apiAuthorization, m_standingsTimer, this, useAlliance, useCorp, usePersonal)).start();
                m_standingsRefreshRunning.compareAndSet(false, true);
                if (m_logger.isDebugEnabled()) {
                    m_logger.debug("StandingsTimer running");
                }
            }
        }
    }

    private void reset() {
        m_characterName = null;
        m_corporationId = 0L;
        m_allianceId = 0L;
        if (m_standingsRefreshRunning.getAndSet(false)) {
            m_standingsTimer.cancel();
            m_standingsTimer = new Timer("StandingsTimer");
        }
        m_standings = null;
    }

    @Override
    public void notifyConfigChange() {
        try {
            start();
        }
        catch (Exception e) {
            m_logger.error("CurrentUserContext failed to rebuild", e);
        }
    }

    @Override
    public String getPilotName() {
        return m_characterName;
    }

    @Override
    public Long getCorporationId() {
        return m_corporationId;
    }

    @Override
    public Long getAllianceId() {
        return m_allianceId;
    }

    @Override
    public EntityStatus getStanding(Long eveId) {
        EntityStatus result = null;
        if (m_standings != null) {
            Double value = m_standings.get(eveId);
            if (value != null) {
                if (value < -5) {
                    result = EntityStatus.RED_10;
                }
                else if (value < 0) {
                    result = EntityStatus.RED_5;
                }
                else if (value == 0) {
                    result = EntityStatus.NEUTRAL_STANDINGS;
                }
                else if (value > 5) {
                    result = EntityStatus.BLUE_10;
                }
                else if (value > 0) {
                    result = EntityStatus.BLUE_5;
                }
            }
        }
        if (m_logger.isDebugEnabled()) {
            m_logger.debug("Result for " + eveId + " = " + (result == null ? "null" : result));
        }
        return result;
    }

    public void setStandings(Map<Long, Double> standings) {
        m_standings = standings;
    }

    @Override
    public boolean hasNegativeStandings(KOSPilotResultDTO pilot) {
        boolean result = false;
        if (pilot != null && m_standings != null) {
            result = (m_standings.get(pilot.getEveId()) != null && m_standings.get(pilot.getEveId()) < 0);
            if (!result && pilot.getCorp() != null) {
                result = (m_standings.get(pilot.getCorp().getEveId()) != null && m_standings.get(pilot.getCorp().getEveId()) < 0);
                if (!result && pilot.getCorp().getAlliance() != null) {
                    result = (m_standings.get(pilot.getCorp().getAlliance().getEveId()) != null && m_standings.get(pilot.getCorp().getAlliance().getEveId()) < 0);
                }
            }
        }
        return result;
    }
}
