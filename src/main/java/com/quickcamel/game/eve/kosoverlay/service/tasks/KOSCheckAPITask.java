/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.tasks;

import com.quickcamel.game.eve.kosoverlay.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

/**
 * Description: A Task that actually goes to the CVA webservice to check the KOS status
 *
 * @author Louis Burton
 */
public class KOSCheckAPITask extends KOSCheckTask {

    private static final Logger m_logger = LoggerFactory.getLogger(KOSCheckAPITask.class);

    @Inject
    private IKOSChecker m_kosChecker;


    @Override
    protected void doCheck() throws Exception {
        long start = System.currentTimeMillis();
        try {
            m_kosResult = m_kosChecker.checkPilot(m_pilotName);
        }
        catch (Throwable e) {
            m_logger.error("KOS API Check failed for " + m_pilotName, e);
            throw e;
        }
        finally {
            long duration = System.currentTimeMillis() - start;
            if (duration > 5000) {
                m_logger.warn("Check for pilot " + m_pilotName + " took : " + duration);
            }
        }
    }
}
