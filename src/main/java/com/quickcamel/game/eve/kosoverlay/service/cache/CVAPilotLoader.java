/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.cache;

import com.google.common.cache.CacheLoader;
import com.quickcamel.game.eve.kosoverlay.configuration.Version;
import com.quickcamel.game.eve.kosoverlay.service.CVARESTLogger;
import com.quickcamel.game.eve.kosoverlay.service.dto.*;
import com.quickcamel.game.eve.kosoverlay.service.rest.IRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.*;

/**
 * Loads a Pilot from the CVA API
 *
 * @author Louis Burton
 */
public class CVAPilotLoader extends CacheLoader<String, KOSResultContainerDTO> {

    private static final Logger m_logger = LoggerFactory.getLogger(CVAPilotLoader.class);

    private static final int CVA_RETRY_COUNT = 3;

    @Inject
    private IRestClient m_restClient;

    @Inject
    private Version m_version;

    public KOSResultContainerDTO load(String pilotName) throws LoadingException {
        m_logger.debug("Loading Pilot information for " + pilotName);
        KOSResultContainerDTO result = null;
        Exception lastException = null;
        for (int i = 0; i < CVA_RETRY_COUNT && result == null; ++i) {
            if (i > 0) {
                try {
                    Thread.sleep(1000);
                }
                catch (InterruptedException e) {
                    throw new LoadingException(e);
                }
            }
            try {
                Map<String, List<String>> queryParams = new HashMap<>();
                queryParams.put("c", Arrays.asList("json"));
                queryParams.put("type", Arrays.asList("pilot"));
                queryParams.put("icon", Arrays.asList("32"));
                queryParams.put("max", Arrays.asList("1"));
                queryParams.put("offset", Arrays.asList("0"));
                queryParams.put("q", Arrays.asList(pilotName));
                Map<String, String> headerParams = new HashMap<>();
                headerParams.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON);
                headerParams.put(HttpHeaders.USER_AGENT, m_version.getUserAgent());
                result = m_restClient.get("http://kos.cva-eve.org/api/", queryParams, headerParams, KOSResultContainerDTO.class);
                if (m_logger.isDebugEnabled()) {
                    m_logger.debug("Request: " + Version.REQUEST_COUNTER.incrementAndGet() + " Entity: " + pilotName + " Datetime: " + System.currentTimeMillis());
                }
            }
            catch (Exception e) {
                if (e.getCause() instanceof WebApplicationException) {
                    CVARESTLogger.handleWebApplicationException("Pilot " + pilotName + " attempt " + (i + 1), (WebApplicationException) e.getCause());
                }
                lastException = e;
                m_logger.error("API Check for " + pilotName + " failed - attempt #" + (i + 1), e);
            }
        }
        if (m_logger.isDebugEnabled()) {
            m_logger.debug("Result := " + result);
        }
        if (result == null
                || result.getCode() != 100) {
            if (lastException != null) {
                throw new LoadingException("Unable to load Pilot data for " + pilotName + " from CVA", lastException);
            }
            else {
                throw new LoadingException("Unable to load Pilot data for " + pilotName + " from CVA");
            }
        }
        return result;
    }
}
