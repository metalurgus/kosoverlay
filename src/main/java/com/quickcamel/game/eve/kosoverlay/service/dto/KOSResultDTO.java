/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlElement;

/**
 * Description: Base properties present in all of the CVA API's result objects
 *
 * @author Louis Burton
 */
public abstract class KOSResultDTO {

    protected String m_type;
    protected Long m_id;
    protected String m_label;
    protected String m_icon;
    protected Boolean m_kos;
    protected Long m_eveId;

    public String getType() {
        return m_type;
    }

    public void setType(String type) {
        m_type = type;
    }

    public Long getId() {
        return m_id;
    }

    public void setId(Long id) {
        m_id = id;
    }

    public String getLabel() {
        return m_label;
    }

    public void setLabel(String label) {
        m_label = label;
    }

    public String getIcon() {
        return m_icon;
    }

    public void setIcon(String icon) {
        m_icon = icon;
    }

    public Boolean getKos() {
        return m_kos;
    }

    public void setKos(Boolean kos) {
        m_kos = kos;
    }

    @XmlElement(name = "eveid")
    public Long getEveId() {
        return m_eveId;
    }

    public void setEveId(Long eveId) {
        m_eveId = eveId;
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("type", m_type).
                append("id", m_id).
                append("label", m_label).
                append("icon", m_icon).
                append("kos", m_kos).
                append("eveid", m_eveId).
                toString();
    }
}
