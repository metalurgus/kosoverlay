/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.cache;

import com.beimin.eveapi.model.eve.CharacterLookup;
import com.beimin.eveapi.parser.eve.CharacterLookupParser;
import com.beimin.eveapi.response.eve.CharacterLookupResponse;
import com.google.common.cache.CacheLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Loads Name for a Eve Id from the EVE API
 *
 * @author Louis Burton
 */
public class IdToNameLoader extends CacheLoader<Long, String> {

    private static final Logger m_logger = LoggerFactory.getLogger(NameToIdLoader.class);

    @Override
    public String load(Long eveId) throws LoadingException {
        m_logger.debug("Loading name from EVE API for " + eveId);
        String errorMsg = "Unable to load name from EVE API for " + eveId;
        try {
            CharacterLookupResponse response = CharacterLookupParser.getId2NameInstance().getResponse(eveId);
            if (response.hasError() || response.getAll() == null || response.getAll().size() == 0) {
                m_logger.warn(errorMsg);
                if (response.hasError() && response.getError() != null) {
                    m_logger.warn(response.getError().toString());
                }
            }
            else {
                CharacterLookup lookup = response.getAll().iterator().next();
                if (lookup.getName() == null || lookup.getName().length() == 0) {
                    m_logger.warn(errorMsg);
                }
                else {
                    if (m_logger.isDebugEnabled()) {
                        m_logger.debug("Name for eveId " + eveId + " : " + lookup.getName());
                    }
                    return lookup.getName();
                }
            }
        }
        catch (Exception e) {
            m_logger.error(errorMsg, e);
        }
        throw new LoadingException(errorMsg);
    }
}
