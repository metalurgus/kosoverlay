/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service;

import com.quickcamel.game.eve.kosoverlay.service.dto.*;
import org.apache.commons.lang3.StringUtils;

/**
 * Populates a readable short reason with Alliance > Corp > Pilot precedence, updates status of all items
 *
 * @author Louis Burton
 */
public class KOSSummaryHelper implements IKOSSummaryHelper {

    private static final String NOT_AVAILABLE = "N/A";

    @Override
    public void updateSummaryFromPilot(KOSPilotResultDTO pilot, KOSStatusSummary kosStatusSummary) {
        if (pilot.getCorp() != null) {
            // This will update from corp, and corp's alliance respectively if needed
            updateSummaryFromCorp(pilot.getCorp(), kosStatusSummary, false);
        }
        if (!Boolean.TRUE.equals(pilot.getKos())) {
            kosStatusSummary.setPilotStatus(EntityStatus.NEUTRAL);
        }
        else {
            // If a higher player entity exists and is not KOS,
            // there is an inconsistency here in the CVA KOS system that the user should be weary of
            if (pilot.getCorp() != null
                    && !pilot.getCorp().getNpc()
                    && !pilot.getCorp().getLabel().equals("None")
                    && !kosStatusSummary.getResolvedStatus().equals(EntityStatus.KOS_CVA)) {
                kosStatusSummary.setPilotStatus(EntityStatus.KOS_CVA_CONFLICT);
            }
            else {
                kosStatusSummary.setPilotStatus(EntityStatus.KOS_CVA);
            }
            if (kosStatusSummary.getShortReason() == null) {
                kosStatusSummary.setShortReason("(P)");
            }
        }
    }

    @Override
    public void updateSummaryFromCorp(KOSCorpResultDTO corp, KOSStatusSummary kosStatusSummary, boolean lastCorp) {
        if (corp == null) {
            throw new IllegalStateException("Corp must be provided for status summary update");
        }
        if (corp.getAlliance() != null) {
            updateSummaryFromAlliance(corp.getAlliance(), kosStatusSummary, lastCorp);
        }
        if (!Boolean.TRUE.equals(corp.getKos())) {
            if (lastCorp) {
                // KOS by last corp is associated with the pilot (but don't override if already KOS by last alliance)
                if (EntityStatus.NEUTRAL.overrules(kosStatusSummary.getPilotStatus())) {
                    kosStatusSummary.setPilotStatus(EntityStatus.NEUTRAL);
                }
            }
            else {
                kosStatusSummary.setCorpStatus(EntityStatus.NEUTRAL);
            }
        }
        else {
            EntityStatus kosStatus;
            // If a higher entity exists and is not KOS,
            // there is an inconsistency here in the CVA KOS system that the user should be weary of
            if (corp.getAlliance() != null
                    && !corp.getAlliance().getLabel().equals("None")
                    && !kosStatusSummary.getResolvedStatus().equals(EntityStatus.KOS_CVA)) {
                kosStatus = EntityStatus.KOS_CVA_CONFLICT;
            }
            else {
                kosStatus = EntityStatus.KOS_CVA;
            }
            if (lastCorp) {
                // KOS by last corp is associated with the pilot
                kosStatusSummary.setPilotStatus(kosStatus);
            }
            else {
                kosStatusSummary.setCorpStatus(kosStatus);
            }
            if (kosStatusSummary.getShortReason() == null) {
                StringBuilder shortReason = new StringBuilder(10);
                shortReason.append('(');
                if (lastCorp) {
                    shortReason.append('L');
                }
                shortReason.append("C:");
                if (StringUtils.isEmpty(corp.getTicker())) {
                    shortReason.append(NOT_AVAILABLE);
                }
                else {
                    shortReason.append(corp.getTicker());
                }
                shortReason.append(')');
                kosStatusSummary.setShortReason(shortReason.toString());
            }
        }
    }

    @Override
    public void updateSummaryFromAlliance(KOSAllianceResultDTO alliance, KOSStatusSummary kosStatusSummary,
                                          boolean lastAlliance) {
        if (alliance == null) {
            throw new IllegalStateException("Alliance must be provided for status summary update");
        }
        if (!Boolean.TRUE.equals(alliance.getKos())) {
            if (lastAlliance) {
                // KOS by last alliance is associated with the pilot
                kosStatusSummary.setPilotStatus(EntityStatus.NEUTRAL);
            }
            else {
                kosStatusSummary.setAllianceStatus(EntityStatus.NEUTRAL);
            }
        }
        else {
            if (lastAlliance) {
                // KOS by last alliance is associated with the pilot
                kosStatusSummary.setPilotStatus(EntityStatus.KOS_CVA);
            }
            else {
                kosStatusSummary.setAllianceStatus(EntityStatus.KOS_CVA);
            }
            if (kosStatusSummary.getShortReason() == null) {
                StringBuilder shortReason = new StringBuilder(10);
                shortReason.append('(');
                if (lastAlliance) {
                    shortReason.append('L');
                }
                shortReason.append("A:");
                if (StringUtils.isEmpty(alliance.getTicker())) {
                    shortReason.append(NOT_AVAILABLE);
                }
                else {
                    shortReason.append(alliance.getTicker());
                }
                shortReason.append(')');
                kosStatusSummary.setShortReason(shortReason.toString());
            }
        }
    }
}
