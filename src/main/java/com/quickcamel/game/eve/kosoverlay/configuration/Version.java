/*
 * Copyright (c) 2016.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.configuration;

import com.quickcamel.game.eve.kosoverlay.service.user.ICurrentUserContext;

import javax.inject.Inject;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Description: Version information
 *
 * @author Louis Burton
 */
public class Version {

    public static final String VERSION = "v1.2.1";
    public static final AtomicInteger REQUEST_COUNTER = new AtomicInteger(0);

    @Inject
    private ICurrentUserContext m_userContext;

    public String getUserAgent() {
        return "KOSOverlay " + VERSION + " louis.burton@gmail.com" + (m_userContext.getPilotName() != null ? " " + m_userContext.getPilotName() : "");
    }
}
