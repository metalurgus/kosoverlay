/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.cache;

import com.google.common.cache.CacheLoader;
import com.quickcamel.game.eve.kosoverlay.configuration.Version;
import com.quickcamel.game.eve.kosoverlay.service.CVARESTLogger;
import com.quickcamel.game.eve.kosoverlay.service.dto.*;
import com.quickcamel.game.eve.kosoverlay.service.rest.IRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.*;

/**
 * Loads an Alliance from the CVA API
 *
 * @author Louis Burton
 */
public class CVAAllianceLoader extends CacheLoader<String, KOSResultContainerDTO> {

    private static final Logger m_logger = LoggerFactory.getLogger(CVAAllianceLoader.class);

    @Inject
    private IRestClient m_restClient;

    @Inject
    private Version m_version;

    public KOSResultContainerDTO load(String alliance) throws LoadingException {
        m_logger.debug("Loading Alliance information for " + alliance);
        KOSResultContainerDTO result;
        try {
            Map<String, List<String>> queryParams = new HashMap<>();
            queryParams.put("c", Arrays.asList("json"));
            queryParams.put("type", Arrays.asList("alliance"));
            queryParams.put("icon", Arrays.asList("32"));
            queryParams.put("max", Arrays.asList("1"));
            queryParams.put("offset", Arrays.asList("0"));
            queryParams.put("q", Arrays.asList(alliance));
            Map<String, String> headerParams = new HashMap<>();
            headerParams.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON);
            headerParams.put(HttpHeaders.USER_AGENT, m_version.getUserAgent());
            result = m_restClient.get("http://kos.cva-eve.org/api/", queryParams, headerParams, KOSResultContainerDTO.class);
            if (m_logger.isDebugEnabled()) {
                m_logger.debug("Request: " + Version.REQUEST_COUNTER.incrementAndGet() + " Entity: " + alliance + " Datetime: " + System.currentTimeMillis());
            }
        }
        catch (Exception e) {
            if (e.getCause() instanceof WebApplicationException) {
                CVARESTLogger.handleWebApplicationException("Alliance " + alliance, (WebApplicationException) e.getCause());
            }
            throw new LoadingException("Unable to load Alliance data for " + alliance, e);
        }
        if (m_logger.isDebugEnabled()) {
            m_logger.debug("Result := " + result);
        }
        if (result == null
                || result.getCode() != 100) {
            throw new LoadingException("Unable to load Alliance data for " + alliance + " from CVA");
        }
        return result;
    }
}
