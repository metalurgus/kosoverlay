/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view.fx.controllers;

import com.quickcamel.game.eve.kosoverlay.KOSOverlayLauncher;
import com.quickcamel.game.eve.kosoverlay.configuration.Version;
import com.quickcamel.game.eve.kosoverlay.view.fx.UIConstants;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller functionality for the settings tab - orientated around accreditation considerations
 *
 * @author Louis Burton
 */
public class SettingsTabAboutController {

    private static final Logger m_logger = LoggerFactory.getLogger(SettingsTabGeneralController.class);

    @FXML
    private Label m_title;

    @FXML
    private Label m_repositoryText;

    @FXML
    private Hyperlink m_repositoryURL;

    @FXML
    private Label m_apiText;

    @FXML
    private Hyperlink m_apiURL;

    @FXML
    private Label m_developer;

    private KOSOverlayLauncher m_launcher;

    public void registerLauncher(KOSOverlayLauncher launcher) {
        m_launcher = launcher;
    }

    @FXML
    public void initialize() {
        m_title.setText("KOSOverlay " + Version.VERSION);
        m_title.getStyleClass().add(UIConstants.TAB_CONTENT_TITLE_STYLE);
        m_repositoryURL.getStyleClass().add(UIConstants.HYPERLINK_STYLE);
        m_repositoryText.setText("Repository:");
        m_repositoryURL.setText("https://bitbucket.org/louisburton/kosoverlay");
        m_repositoryURL.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                m_launcher.getHostServices().showDocument(m_repositoryURL.getText());
            }
        });
        m_apiText.setText("KOS API:");
        m_apiURL.setText("http://kos.cva-eve.org/");
        m_apiURL.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                m_launcher.getHostServices().showDocument(m_apiURL.getText());
            }
        });
        m_developer.setText("Developer:\n" +
                "Louis Burton <louis.burton@gmail.com>");
    }
}
