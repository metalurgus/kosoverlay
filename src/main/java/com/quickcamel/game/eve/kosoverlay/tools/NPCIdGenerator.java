package com.quickcamel.game.eve.kosoverlay.tools;

import com.beimin.eveapi.parser.eve.CharacterLookupParser;
import com.beimin.eveapi.response.eve.CharacterLookupResponse;
import com.quickcamel.game.eve.kosoverlay.service.KOSChecker;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.util.*;

/**
 * Generate NPC Corporation Ids
 *
 * @author Louis Burton
 */
public class NPCIdGenerator {

    public static void main(String[] args) throws Exception {
        List<String> npcCorps = IOUtils.readLines(KOSChecker.class.getResourceAsStream("npc-corporations.txt"));
        Set<Long> npcIds = new TreeSet<>();
        for (String npcCorp : npcCorps) {
            CharacterLookupResponse response = CharacterLookupParser.getName2IdInstance().getResponse(npcCorp);
            if (response.getAll().size() > 0) {
                Long corpId = response.getAll().iterator().next().getCharacterID();
                npcIds.add(corpId);
                System.out.println(npcCorp + " : " + corpId);
            }
        }
        FileUtils.writeLines(File.createTempFile("npc-corporation-ids-", ".txt"), npcIds);
    }
}
