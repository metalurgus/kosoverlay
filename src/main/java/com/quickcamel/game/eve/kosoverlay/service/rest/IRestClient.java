/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.rest;

import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Description:
 *
 * @author Louis Burton
 */
public interface IRestClient {


    /**
     * Method that invoke the GET method.
     *
     * @param path             full path including subpath of the resource
     * @param queryParams      the query parameters single key and multiple values map
     * @param headerParams     the header parameters key value map
     * @param returnEntityType an instance of type represented by the generic class type
     * @param <T>              the type of the response.
     * @return an instance of type <code>returnEntityType</code>
     * @throws RestClientException
     */
    <T> T get(String path, Map<String, List<String>> queryParams, Map<String, String> headerParams,
              Class<T> returnEntityType) throws RestClientException;



    /**
     * Method that invokes the POST method with form params, does not follow redirects.
     *
     * @param path       full path including subpath of the resource
     * @param formParams the form parameters map. Supports single key, single value
     * @return URI the location returned
     * @throws RestClientException
     */
    URI post(String path, Map<String, String> formParams) throws RestClientException;

}
