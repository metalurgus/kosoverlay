/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view.fx;

import javafx.application.Preloader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Description: Controls the initial splash screen
 *
 * @author Louis Burton
 */
public class KOSPreloader extends Preloader {

    private static final Logger m_logger = LoggerFactory.getLogger(KOSPreloader.class);

    private ProgressBar m_progressBar;
    private Stage m_stage;

    @Override
    public void start(Stage stage) throws Exception {
        this.m_stage = stage;
        stage.setScene(createPreloaderScene());
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
    }

    private Scene createPreloaderScene() {
        m_progressBar = new ProgressBar();
        m_progressBar.setMinWidth(312);
        m_progressBar.setMaxWidth(312);
        m_progressBar.setMinHeight(13);
        m_progressBar.setMaxHeight(13);
        m_progressBar.setStyle("-fx-border : 1px; -fx-border-color: black;");
        ImageView imageView = new ImageView(new Image(this.getClass().getResourceAsStream("sev-splash.png")));
        VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        vbox.getChildren().addAll(imageView, m_progressBar);
        return new Scene(vbox, 312, 170);
    }

    boolean m_receivedLoadingProgress = false;

    @Override
    public void handleProgressNotification(ProgressNotification pn) {
        //application loading progress is rescaled to be first 50%
        //Even if there is nothing to load 0% and 100% events can be delivered
        if (pn.getProgress() != 1.0 || m_receivedLoadingProgress) {
            m_progressBar.setProgress(pn.getProgress() / 2);
            m_logger.debug("Progress " + m_progressBar.getProgress());
            if (pn.getProgress() > 0) {
                m_receivedLoadingProgress = true;
            }
        }
    }

    @Override
    public void handleStateChangeNotification(StateChangeNotification evt) {
        if (StateChangeNotification.Type.BEFORE_START == evt.getType()) {
            m_receivedLoadingProgress = true;
            m_progressBar.setProgress(0.5);
            m_logger.debug("Progress " + m_progressBar.getProgress());
        }
        //ignore, hide after application signals it is ready
    }

    @Override
    public void handleApplicationNotification(PreloaderNotification pn) {
        if (pn instanceof ProgressNotification) {
            //expect application to send us progress notifications
            //with progress ranging from 0 to 1.0
            double v = ((ProgressNotification) pn).getProgress();
            if (m_receivedLoadingProgress) {
                //if we were receiving loading progress notifications
                //then progress is already at 50%.
                //Rescale application progress to start from 50%
                v = 0.5 + v / 2;
            }
            m_progressBar.setProgress(v);
            m_logger.debug("Progress " + m_progressBar.getProgress());
        }
        else if (pn instanceof StateChangeNotification) {
            //hide after get any state update from application
            m_logger.debug("Hiding splash " + m_progressBar.getProgress());
            m_stage.hide();
        }
    }
}
