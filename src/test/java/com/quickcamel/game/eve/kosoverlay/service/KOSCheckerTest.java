/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service;

import com.beimin.eveapi.model.eve.CharacterEmployment;
import com.beimin.eveapi.response.corporation.CorpSheetResponse;
import com.beimin.eveapi.response.eve.CharacterInfoResponse;
import com.google.common.cache.LoadingCache;
import com.quickcamel.game.eve.kosoverlay.configuration.IConfigManager;
import com.quickcamel.game.eve.kosoverlay.service.cache.LoadingException;
import com.quickcamel.game.eve.kosoverlay.service.dto.*;
import com.quickcamel.game.eve.kosoverlay.service.user.ICurrentUserContext;
import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.*;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for the KOSChecker (including its KOSSummaryHelper utility)
 *
 * @author Louis Burton
 */
public class KOSCheckerTest {

    @Rule
    public JUnitRuleMockery m_mockery = new JUnitRuleMockery();

    @Mock
    private IConfigManager m_configManager;
    @Mock
    private LoadingCache<String, KOSResultContainerDTO> m_pilotCVACache;
    @Mock
    private LoadingCache<String, KOSResultContainerDTO> m_corpCVACache;
    @Mock
    private LoadingCache<String, KOSResultContainerDTO> m_allianceCVACache;
    @Mock
    private LoadingCache<String, Long> m_nameToIdCache;
    @Mock
    private LoadingCache<Long, CharacterInfoResponse> m_idToCharacterInfoCache;
    @Mock
    private LoadingCache<Long, String> m_idToNameCache;
    @Mock
    private LoadingCache<Long, CorpSheetResponse> m_corpIdToCorpSheetCache;
    @Mock
    private ICurrentUserContext m_userContext;

    private IKOSSummaryHelper m_kosSummaryHelper = new KOSSummaryHelper();

    private IKOSChecker m_kosChecker = new KOSChecker();

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(m_kosChecker, "m_configManager", m_configManager, IConfigManager.class);
        ReflectionTestUtils.setField(m_kosChecker, "m_pilotCVACache", m_pilotCVACache, LoadingCache.class);
        ReflectionTestUtils.setField(m_kosChecker, "m_corpCVACache", m_corpCVACache, LoadingCache.class);
        ReflectionTestUtils.setField(m_kosChecker, "m_allianceCVACache", m_allianceCVACache, LoadingCache.class);
        ReflectionTestUtils.setField(m_kosChecker, "m_nameToIdCache", m_nameToIdCache, LoadingCache.class);
        ReflectionTestUtils.setField(m_kosChecker, "m_idToCharacterInfoCache", m_idToCharacterInfoCache, LoadingCache.class);
        ReflectionTestUtils.setField(m_kosChecker, "m_idToNameCache", m_idToNameCache, LoadingCache.class);
        ReflectionTestUtils.setField(m_kosChecker, "m_corpIdToCorpSheetCache", m_corpIdToCorpSheetCache, LoadingCache.class);
        ReflectionTestUtils.setField(m_kosChecker, "m_userContext", m_userContext, ICurrentUserContext.class);
        ReflectionTestUtils.setField(m_kosChecker, "m_kosSummaryHelper", m_kosSummaryHelper, IKOSSummaryHelper.class);

        m_mockery.checking(new Expectations() {{
            // Testing all configuration options set to true
            allowing(m_configManager).getValue(with(any(String.class)), with(any(String.class)));
            will(returnValue("true"));
        }});
    }

    /**
     * KOS - BASIC
     */
    @Test
    public void shouldCVAKOSIfPilotKOSInNPCCorp() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(true);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setNpc(true);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Pilot's NPC Corp");
        pilotResultDTO.setCorp(corpResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        m_mockery.checking(new Expectations() {{
            // The Pilot found immediately on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.KOS_CVA, result.getStatusSummary().getResolvedStatus());
        assertEquals("(P)", result.getStatusSummary().getShortReason());
    }

    @Test
    public void shouldCVAKOSIfPilotNeutInKOSCorp() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(false);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setKos(true);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Pilot's Corp");
        corpResultDTO.setTicker("0UCH");
        pilotResultDTO.setCorp(corpResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        m_mockery.checking(new Expectations() {{
            // The Pilot found immediately on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.KOS_CVA, result.getStatusSummary().getResolvedStatus());
        assertEquals("(C:0UCH)", result.getStatusSummary().getShortReason());
    }

    @Test
    public void shouldCVAKOSIfPilotAndCorpNeutInKOSAlliance() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(false);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setKos(false);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Pilot's Corp");
        corpResultDTO.setTicker("0UCH");
        pilotResultDTO.setCorp(corpResultDTO);
        KOSAllianceResultDTO allianceResultDTO = new KOSAllianceResultDTO();
        allianceResultDTO.setKos(true);
        allianceResultDTO.setType("alliance");
        allianceResultDTO.setLabel("Pilot's Corp's Alliance");
        allianceResultDTO.setTicker("AWA");
        corpResultDTO.setAlliance(allianceResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        m_mockery.checking(new Expectations() {{
            // The Pilot found immediately on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.KOS_CVA, result.getStatusSummary().getResolvedStatus());
        assertEquals("(A:AWA)", result.getStatusSummary().getShortReason());
    }

    @Test
    public void shouldCVAKOSIfPilotCorpAndAllianceKOS() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(true);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setKos(true);
        corpResultDTO.setNpc(false);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Pilot's Corp");
        corpResultDTO.setTicker("0UCH");
        pilotResultDTO.setCorp(corpResultDTO);
        KOSAllianceResultDTO allianceResultDTO = new KOSAllianceResultDTO();
        allianceResultDTO.setKos(true);
        allianceResultDTO.setType("alliance");
        allianceResultDTO.setLabel("Pilot's Corp's Alliance");
        allianceResultDTO.setTicker("AWA");
        corpResultDTO.setAlliance(allianceResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        m_mockery.checking(new Expectations() {{
            // The Pilot found immediately on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.KOS_CVA, result.getStatusSummary().getResolvedStatus());
        assertEquals("(A:AWA)", result.getStatusSummary().getShortReason());
    }

    /**
     * KOS - NO RESULT SCENARIOS
     */
    @Test
    public void shouldCVAKOSIfNoResultsPilotInKOSCorp() throws Exception {
        // Setup what CVA returns in this test
        // CVA Pilot Result with 'No Results'
        final KOSResultContainerDTO pilotContainerResult = new KOSResultContainerDTO();
        pilotContainerResult.setTotal(0);
        pilotContainerResult.setCode(100);
        // CVA Corp Result
        final KOSResultContainerDTO corpContainerResult = new KOSResultContainerDTO();
        corpContainerResult.setTotal(1);
        corpContainerResult.setCode(100);
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setKos(true);
        corpResultDTO.setNpc(false);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("CorpFoundByCVA");
        corpResultDTO.setTicker("CFBC");
        corpContainerResult.setResults(Arrays.asList(new KOSResultDTO[]{corpResultDTO}));
        // EVE API Pilot Result
        final CharacterInfoResponse eveCharacterResponse = new CharacterInfoResponse();
        eveCharacterResponse.setCorporation("CorpFoundByCVA");

        m_mockery.checking(new Expectations() {{
            // The Pilot not found on CVA
            allowing(m_pilotCVACache).get("PilotNoResultsCVA");
            will(returnValue(pilotContainerResult));
            // The Corp found on CVA
            allowing(m_corpCVACache).get("CorpFoundByCVA");
            will(returnValue(corpContainerResult));

            // Allow EVE API lookups - no assertions for this test
            allowing(m_nameToIdCache).get(with(any(String.class)));
            will(returnValue(0L));
            allowing(m_idToCharacterInfoCache).get(with(any(Long.class)));
            will(returnValue(eveCharacterResponse));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotNoResultsCVA");
        assertEquals(EntityStatus.KOS_CVA, result.getStatusSummary().getResolvedStatus());
        assertEquals("(C:CFBC)", result.getStatusSummary().getShortReason());
    }

    @Test
    public void shouldCVAKOSIfNoResultsPilotInNoResultsCorpInKOSAlliance() throws Exception {
        // Setup what CVA returns in this test
        // CVA Pilot Result with 'No Results'
        final KOSResultContainerDTO pilotContainerResult = new KOSResultContainerDTO();
        pilotContainerResult.setTotal(0);
        pilotContainerResult.setCode(100);
        // CVA Corp Result with 'No Results'
        final KOSResultContainerDTO corpContainerResult = new KOSResultContainerDTO();
        corpContainerResult.setTotal(0);
        corpContainerResult.setCode(100);
        // CVA Alliance Result
        final KOSResultContainerDTO allianceContainerResult = new KOSResultContainerDTO();
        allianceContainerResult.setTotal(1);
        allianceContainerResult.setCode(100);
        KOSAllianceResultDTO allianceResultDTO = new KOSAllianceResultDTO();
        allianceResultDTO.setKos(true);
        allianceResultDTO.setType("alliance");
        allianceResultDTO.setLabel("AllianceFoundByCVA");
        allianceResultDTO.setTicker("AFBC");
        allianceContainerResult.setResults(Arrays.asList(new KOSResultDTO[]{allianceResultDTO}));
        // EVE API Pilot Result
        final CharacterInfoResponse eveCharacterResponse = new CharacterInfoResponse();
        eveCharacterResponse.setCorporation("CorpNotFoundByCVA");
        eveCharacterResponse.setAlliance("AllianceFoundByCVA");

        m_mockery.checking(new Expectations() {{
            // The Pilot not found on CVA
            allowing(m_pilotCVACache).get("PilotNoResultsCVAInCorpNoResultsCVA");
            will(returnValue(pilotContainerResult));
            // The Corp not found on CVA
            allowing(m_corpCVACache).get("CorpNotFoundByCVA");
            will(returnValue(corpContainerResult));
            // The Alliance found on CVA
            allowing(m_allianceCVACache).get("AllianceFoundByCVA");
            will(returnValue(allianceContainerResult));

            // Allow EVE API lookups - no assertions for this test
            allowing(m_nameToIdCache).get(with(any(String.class)));
            will(returnValue(0L));
            allowing(m_idToCharacterInfoCache).get(with(any(Long.class)));
            will(returnValue(eveCharacterResponse));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotNoResultsCVAInCorpNoResultsCVA");
        assertEquals(EntityStatus.KOS_CVA, result.getStatusSummary().getResolvedStatus());
        assertEquals("(A:AFBC)", result.getStatusSummary().getShortReason());
    }

    /**
     * KOS - CONFLICT SCENARIOS
     */
    @Test
    public void shouldCVAKOSConflictIfPilotAndCorpKOSInNeutAlliance() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(true);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setKos(true);
        corpResultDTO.setNpc(false);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Pilot's Corp");
        corpResultDTO.setTicker("0UCH");
        pilotResultDTO.setCorp(corpResultDTO);
        KOSAllianceResultDTO allianceResultDTO = new KOSAllianceResultDTO();
        allianceResultDTO.setKos(false);
        allianceResultDTO.setType("alliance");
        allianceResultDTO.setLabel("Pilot's Corp's Alliance");
        allianceResultDTO.setTicker("AWA");
        corpResultDTO.setAlliance(allianceResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        m_mockery.checking(new Expectations() {{
            // The Pilot found immediately on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.KOS_CVA_CONFLICT, result.getStatusSummary().getResolvedStatus());
        assertEquals("(C:0UCH)", result.getStatusSummary().getShortReason());
    }

    @Test
    public void shouldCVAKOSConflictIfKOSPilotInNeutCorp() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(true);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setKos(false);
        corpResultDTO.setNpc(false);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Pilot's Corp");
        corpResultDTO.setTicker("0UCH");
        pilotResultDTO.setCorp(corpResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        m_mockery.checking(new Expectations() {{
            // The Pilot found immediately on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.KOS_CVA_CONFLICT, result.getStatusSummary().getResolvedStatus());
        assertEquals("(P)", result.getStatusSummary().getShortReason());
    }

    /**
     * KOS - LAST CORP/ALLIANCE
     */
    @Test
    public void shouldCVAKOSLastCorp() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(false);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        pilotResultDTO.setEveId(93042718L);
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setNpc(true);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Pilot's NPC Corp");
        corpResultDTO.setEveId(1000109L);
        pilotResultDTO.setCorp(corpResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        // Employment History
        CharacterEmployment currentEmployment = new CharacterEmployment();
        ReflectionTestUtils.setField(currentEmployment, "corporationID", 1000109L, Long.class);
        ReflectionTestUtils.setField(currentEmployment, "startDate", new Date(), Date.class);
        CharacterEmployment oldEmployment = new CharacterEmployment();
        ReflectionTestUtils.setField(currentEmployment, "corporationID", 103373655L, Long.class);
        ReflectionTestUtils.setField(currentEmployment, "startDate", new Date(), Date.class);
        // Historical Corp
        final KOSResultContainerDTO corpContainerResult = new KOSResultContainerDTO();
        corpContainerResult.setTotal(1);
        corpContainerResult.setCode(100);
        KOSCorpResultDTO historicalCorpResultDTO = new KOSCorpResultDTO();
        historicalCorpResultDTO.setKos(true);
        historicalCorpResultDTO.setNpc(false);
        historicalCorpResultDTO.setType("corp");
        historicalCorpResultDTO.setLabel("Open University of Celestial Hardship");
        historicalCorpResultDTO.setTicker("0UCH");
        corpContainerResult.setResults(Arrays.asList(new KOSResultDTO[]{historicalCorpResultDTO}));

        // EVE API Pilot Result
        final CharacterInfoResponse eveCharacterResponse = new CharacterInfoResponse();
        eveCharacterResponse.setCorporation("CorpNotFoundByCVA");
        eveCharacterResponse.setAlliance("AllianceFoundByCVA");
        List<CharacterEmployment> employmentHistory = eveCharacterResponse.getEmploymentHistory();
        employmentHistory.add(currentEmployment);
        employmentHistory.add(oldEmployment);

        m_mockery.checking(new Expectations() {{
            // The Pilot found immediately on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
            // Last Corp found
            allowing(m_corpCVACache).get("Open University of Celestial Hardship");
            will(returnValue(corpContainerResult));

            // Allow EVE API lookups
            allowing(m_idToCharacterInfoCache).get(with(any(Long.class)));
            will(returnValue(eveCharacterResponse));
            allowing(m_idToNameCache).get(103373655L);
            will(returnValue("Open University of Celestial Hardship"));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.KOS_CVA, result.getStatusSummary().getResolvedStatus());
        assertEquals("(LC:0UCH)", result.getStatusSummary().getShortReason());
    }

    @Test
    public void shouldCVAKOSLastAlliance() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(false);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        pilotResultDTO.setEveId(93042718L);
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setNpc(true);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Pilot's NPC Corp");
        corpResultDTO.setEveId(1000109L);
        pilotResultDTO.setCorp(corpResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        // Employment History
        CharacterEmployment currentEmployment = new CharacterEmployment();
        ReflectionTestUtils.setField(currentEmployment, "corporationID", 1000109L, Long.class);
        ReflectionTestUtils.setField(currentEmployment, "startDate", new Date(), Date.class);
        CharacterEmployment oldEmployment = new CharacterEmployment();
        ReflectionTestUtils.setField(currentEmployment, "corporationID", 103373655L, Long.class);
        ReflectionTestUtils.setField(currentEmployment, "startDate", new Date(), Date.class);
        // Historical Corp
        final KOSResultContainerDTO corpContainerResult = new KOSResultContainerDTO();
        corpContainerResult.setTotal(1);
        corpContainerResult.setCode(100);
        KOSCorpResultDTO historicalCorpResultDTO = new KOSCorpResultDTO();
        historicalCorpResultDTO.setKos(false);
        historicalCorpResultDTO.setNpc(false);
        historicalCorpResultDTO.setType("corp");
        historicalCorpResultDTO.setLabel("Good Corp");
        historicalCorpResultDTO.setTicker("GC");
        KOSAllianceResultDTO historicalAllianceResultDTO = new KOSAllianceResultDTO();
        historicalAllianceResultDTO.setKos(true);
        historicalAllianceResultDTO.setType("alliance");
        historicalAllianceResultDTO.setLabel("Art of War Alliance");
        historicalAllianceResultDTO.setTicker("AWA");
        historicalCorpResultDTO.setAlliance(historicalAllianceResultDTO);
        corpContainerResult.setResults(Arrays.asList(new KOSResultDTO[]{historicalCorpResultDTO}));

        // EVE API Pilot Result
        final CharacterInfoResponse eveCharacterResponse = new CharacterInfoResponse();
        eveCharacterResponse.setCorporation("CorpNotFoundByCVA");
        eveCharacterResponse.setAlliance("AllianceFoundByCVA");
        List<CharacterEmployment> employmentHistory = eveCharacterResponse.getEmploymentHistory();
        employmentHistory.add(currentEmployment);
        employmentHistory.add(oldEmployment);

        m_mockery.checking(new Expectations() {{
            // The Pilot found immediately on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
            // Last Corp found
            allowing(m_corpCVACache).get("Good Corp");
            will(returnValue(corpContainerResult));

            // Allow EVE API lookups
            allowing(m_idToCharacterInfoCache).get(with(any(Long.class)));
            will(returnValue(eveCharacterResponse));
            allowing(m_idToNameCache).get(103373655L);
            will(returnValue("Good Corp"));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.KOS_CVA, result.getStatusSummary().getResolvedStatus());
        assertEquals("(LA:AWA)", result.getStatusSummary().getShortReason());
    }

    @Test
    public void shouldCVAKOSIfNoResultLastCorpInKOSLastAlliance() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(false);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        pilotResultDTO.setEveId(93042718L);
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setNpc(true);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Pilot's NPC Corp");
        corpResultDTO.setEveId(1000109L);
        pilotResultDTO.setCorp(corpResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        // Employment History
        CharacterEmployment currentEmployment = new CharacterEmployment();
        ReflectionTestUtils.setField(currentEmployment, "corporationID", 1000109L, Long.class);
        ReflectionTestUtils.setField(currentEmployment, "startDate", new Date(), Date.class);
        CharacterEmployment oldEmployment = new CharacterEmployment();
        ReflectionTestUtils.setField(currentEmployment, "corporationID", 103373655L, Long.class);
        ReflectionTestUtils.setField(currentEmployment, "startDate", new Date(), Date.class);
        // Historical Corp
        final KOSResultContainerDTO corpContainerResult = new KOSResultContainerDTO();
        corpContainerResult.setTotal(0);
        corpContainerResult.setCode(100);
        // Historical Corp's Alliance
        final KOSResultContainerDTO allianceContainerResult = new KOSResultContainerDTO();
        allianceContainerResult.setTotal(1);
        allianceContainerResult.setCode(100);
        KOSAllianceResultDTO historicalAllianceResultDTO = new KOSAllianceResultDTO();
        historicalAllianceResultDTO.setKos(true);
        historicalAllianceResultDTO.setType("alliance");
        historicalAllianceResultDTO.setLabel("Bad Alliance");
        historicalAllianceResultDTO.setTicker("BA");
        allianceContainerResult.setResults(Arrays.asList(new KOSResultDTO[]{historicalAllianceResultDTO}));

        // EVE API Pilot Result
        final CharacterInfoResponse eveCharacterResponse = new CharacterInfoResponse();
        eveCharacterResponse.setCorporation("CorpNotFoundByCVA");
        eveCharacterResponse.setAlliance("AllianceFoundByCVA");
        List<CharacterEmployment> employmentHistory = eveCharacterResponse.getEmploymentHistory();
        employmentHistory.add(currentEmployment);
        employmentHistory.add(oldEmployment);
        // EVE API Corp Result
        final CorpSheetResponse corpSheetResponse = new CorpSheetResponse();
        corpSheetResponse.setCorporationName("Good Corp");
        corpSheetResponse.setAllianceName("Bad Alliance");
        corpSheetResponse.setAllianceID(664167896L);

        m_mockery.checking(new Expectations() {{
            // The Pilot found immediately on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
            // Last Corp not found
            allowing(m_corpCVACache).get("Good Corp");
            will(returnValue(corpContainerResult));
            // Last Alliance found
            allowing(m_allianceCVACache).get("Bad Alliance");
            will(returnValue(allianceContainerResult));

            // Allow EVE API lookups
            allowing(m_idToCharacterInfoCache).get(with(any(Long.class)));
            will(returnValue(eveCharacterResponse));
            allowing(m_idToNameCache).get(103373655L);
            will(returnValue("Good Corp"));
            allowing(m_corpIdToCorpSheetCache).get(103373655L);
            will(returnValue(corpSheetResponse));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.KOS_CVA, result.getStatusSummary().getResolvedStatus());
        assertEquals("(LA:BA)", result.getStatusSummary().getShortReason());
    }

    @Test
    public void shouldCVAKOSConflictIfLastCorpKOSInNeutAlliance() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(false);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        pilotResultDTO.setEveId(93042718L);
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setNpc(true);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Pilot's NPC Corp");
        corpResultDTO.setEveId(1000109L);
        pilotResultDTO.setCorp(corpResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        // Employment History
        CharacterEmployment currentEmployment = new CharacterEmployment();
        ReflectionTestUtils.setField(currentEmployment, "corporationID", 1000109L, Long.class);
        ReflectionTestUtils.setField(currentEmployment, "startDate", new Date(), Date.class);
        CharacterEmployment oldEmployment = new CharacterEmployment();
        ReflectionTestUtils.setField(currentEmployment, "corporationID", 103373655L, Long.class);
        ReflectionTestUtils.setField(currentEmployment, "startDate", new Date(), Date.class);
        // Historical Corp
        final KOSResultContainerDTO corpContainerResult = new KOSResultContainerDTO();
        corpContainerResult.setTotal(1);
        corpContainerResult.setCode(100);
        KOSCorpResultDTO historicalCorpResultDTO = new KOSCorpResultDTO();
        historicalCorpResultDTO.setKos(true);
        historicalCorpResultDTO.setNpc(false);
        historicalCorpResultDTO.setType("corp");
        historicalCorpResultDTO.setLabel("Bad Just Moved Corp");
        historicalCorpResultDTO.setTicker("BJMC");
        KOSAllianceResultDTO historicalAllianceResultDTO = new KOSAllianceResultDTO();
        historicalAllianceResultDTO.setKos(false);
        historicalAllianceResultDTO.setType("alliance");
        historicalAllianceResultDTO.setLabel("Art of War Alliance");
        historicalAllianceResultDTO.setTicker("AWA");
        historicalCorpResultDTO.setAlliance(historicalAllianceResultDTO);
        corpContainerResult.setResults(Arrays.asList(new KOSResultDTO[]{historicalCorpResultDTO}));

        // EVE API Pilot Result
        final CharacterInfoResponse eveCharacterResponse = new CharacterInfoResponse();
        eveCharacterResponse.setCorporation("CorpNotFoundByCVA");
        eveCharacterResponse.setAlliance("AllianceFoundByCVA");
        List<CharacterEmployment> employmentHistory = eveCharacterResponse.getEmploymentHistory();
        employmentHistory.add(currentEmployment);
        employmentHistory.add(oldEmployment);

        m_mockery.checking(new Expectations() {{
            // The Pilot found immediately on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
            // Last Corp found
            allowing(m_corpCVACache).get("Bad Just Moved Corp");
            will(returnValue(corpContainerResult));

            // Allow EVE API lookups
            allowing(m_idToCharacterInfoCache).get(with(any(Long.class)));
            will(returnValue(eveCharacterResponse));
            allowing(m_idToNameCache).get(103373655L);
            will(returnValue("Bad Just Moved Corp"));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.KOS_CVA_CONFLICT, result.getStatusSummary().getResolvedStatus());
        assertEquals("(LC:BJMC)", result.getStatusSummary().getShortReason());
    }

    /**
     * SAME PILOT/CORP/ALLIANCE
     */
    @Test
    public void shouldBlueIfNoResultsPilotInNoResultsCorpInNoResultsAllianceButSamePilot() throws Exception {
        // Setup what CVA returns in this test
        // CVA Pilot Result with 'No Results'
        final KOSResultContainerDTO pilotContainerResult = new KOSResultContainerDTO();
        pilotContainerResult.setTotal(0);
        pilotContainerResult.setCode(100);
        // CVA Corp Result with 'No Results'
        final KOSResultContainerDTO corpContainerResult = new KOSResultContainerDTO();
        corpContainerResult.setTotal(0);
        corpContainerResult.setCode(100);
        // CVA Alliance Result with 'No Result'
        final KOSResultContainerDTO allianceContainerResult = new KOSResultContainerDTO();
        allianceContainerResult.setTotal(0);
        allianceContainerResult.setCode(100);
        // EVE API Pilot Result
        final CharacterInfoResponse eveCharacterResponse = new CharacterInfoResponse();
        eveCharacterResponse.setCharacterName("SamePilotButNoResultsAtAllCVA");
        eveCharacterResponse.setCorporation("CorpNotFoundByCVA");
        eveCharacterResponse.setAlliance("AllianceNotFoundByCVA");

        m_mockery.checking(new Expectations() {{
            // The Pilot not found on CVA
            allowing(m_pilotCVACache).get("SamePilotButNoResultsAtAllCVA");
            will(returnValue(pilotContainerResult));
            // The Corp not found on CVA
            allowing(m_corpCVACache).get("CorpNotFoundByCVA");
            will(returnValue(corpContainerResult));
            // The Alliance not found on CVA
            allowing(m_allianceCVACache).get("AllianceNotFoundByCVA");
            will(returnValue(allianceContainerResult));
            // Same Pilot
            allowing(m_userContext).getPilotName();
            will(returnValue("SamePilotButNoResultsAtAllCVA"));

            // Allow EVE API lookups - no assertions for this test
            allowing(m_nameToIdCache).get(with(any(String.class)));
            will(returnValue(0L));
            allowing(m_idToCharacterInfoCache).get(with(any(Long.class)));
            will(returnValue(eveCharacterResponse));
        }});
        KOSResult result = m_kosChecker.checkPilot("SamePilotButNoResultsAtAllCVA");
        assertEquals(EntityStatus.BLUE_SAME_PILOT_OR_CORP, result.getStatusSummary().getResolvedStatus());
    }

    @Test
    public void shouldBlueIfSameCorp() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(false);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setNpc(false);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("My Corp");
        corpResultDTO.setEveId(103373655L);
        pilotResultDTO.setCorp(corpResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        m_mockery.checking(new Expectations() {{
            // The Pilot found on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
            // Different Pilot
            allowing(m_userContext).getPilotName();
            will(returnValue("Random Pilot"));
            // Same Corp
            allowing(m_userContext).getCorporationId();
            will(returnValue(103373655L));

            // Allow EVE API lookups - no assertions for this test
            allowing(m_nameToIdCache).get(with(any(String.class)));
            will(returnValue(0L));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.BLUE_SAME_PILOT_OR_CORP, result.getStatusSummary().getResolvedStatus());
    }

    @Test
    public void shouldBlueIfSameAlliance() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(false);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setNpc(false);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Random Corp");
        corpResultDTO.setEveId(103373655L);
        pilotResultDTO.setCorp(corpResultDTO);
        KOSAllianceResultDTO allianceResultDTO = new KOSAllianceResultDTO();
        allianceResultDTO.setKos(false);
        allianceResultDTO.setType("alliance");
        allianceResultDTO.setLabel("My Alliance");
        allianceResultDTO.setTicker("MA");
        allianceResultDTO.setEveId(664167896L);
        corpResultDTO.setAlliance(allianceResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        m_mockery.checking(new Expectations() {{
            // The Pilot found on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
            // Different Pilot
            allowing(m_userContext).getPilotName();
            will(returnValue("Random Pilot"));
            // Different Corp
            allowing(m_userContext).getCorporationId();
            will(returnValue(103373999L));
            // Same Alliance
            allowing(m_userContext).getAllianceId();
            will(returnValue(664167896L));

            // Allow EVE API lookups - no assertions for this test
            allowing(m_nameToIdCache).get(with(any(String.class)));
            will(returnValue(0L));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.BLUE_SAME_ALLIANCE, result.getStatusSummary().getResolvedStatus());
    }

    /**
     * STANDINGS
     */
    @Test
    public void shouldStandingsRedWithAlliancePrecedence() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(false);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        pilotResultDTO.setEveId(93042718L);
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setNpc(false);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Random Corp");
        corpResultDTO.setEveId(103373655L);
        pilotResultDTO.setCorp(corpResultDTO);
        KOSAllianceResultDTO allianceResultDTO = new KOSAllianceResultDTO();
        allianceResultDTO.setKos(false);
        allianceResultDTO.setType("alliance");
        allianceResultDTO.setLabel("Random Alliance");
        allianceResultDTO.setEveId(664167896L);
        corpResultDTO.setAlliance(allianceResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        m_mockery.checking(new Expectations() {{
            // The Pilot found on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
            // Different Pilot
            allowing(m_userContext).getPilotName();
            will(returnValue("Random Pilot"));
            // Different Corp
            allowing(m_userContext).getCorporationId();
            will(returnValue(103373999L));
            // Different Alliance
            allowing(m_userContext).getAllianceId();
            will(returnValue(664167999L));
            // Bad Standings - Alliance precedence
            allowing(m_userContext).getStanding(93042718L);
            will(returnValue(EntityStatus.RED_10));
            allowing(m_userContext).getStanding(103373655L);
            will(returnValue(null));
            allowing(m_userContext).getStanding(664167896L);
            will(returnValue(EntityStatus.RED_5));

            // Allow EVE API lookups - no assertions for this test
            allowing(m_nameToIdCache).get(with(any(String.class)));
            will(returnValue(0L));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.RED_5, result.getStatusSummary().getResolvedStatus());
    }

    /**
     * NEUTRAL
     */
    @Test
    public void shouldNeutral() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(false);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        pilotResultDTO.setEveId(93042718L);
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setNpc(false);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Random Corp");
        corpResultDTO.setEveId(103373655L);
        pilotResultDTO.setCorp(corpResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        m_mockery.checking(new Expectations() {{
            // The Pilot found on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
            // Different Pilot
            allowing(m_userContext).getPilotName();
            will(returnValue("Random Pilot"));
            // Different Corp
            allowing(m_userContext).getCorporationId();
            will(returnValue(103373999L));
            // No Standings
            allowing(m_userContext).getStanding(93042718L);
            will(returnValue(null));
            allowing(m_userContext).getStanding(103373655L);
            will(returnValue(null));

            // Allow EVE API lookups - no assertions for this test
            allowing(m_nameToIdCache).get(with(any(String.class)));
            will(returnValue(0L));
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.NEUTRAL, result.getStatusSummary().getResolvedStatus());
    }

    /**
     * ERRORS
     */
    @Test
    public void shouldNeutralWithWarningDueToLastCorpCheck() throws Exception {
        // Setup what CVA returns in this test
        final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
        containerResult.setTotal(1);
        containerResult.setCode(100);
        KOSPilotResultDTO pilotResultDTO = new KOSPilotResultDTO();
        pilotResultDTO.setKos(false);
        pilotResultDTO.setType("pilot");
        pilotResultDTO.setLabel("PilotFoundByCVA");
        pilotResultDTO.setEveId(93042718L);
        KOSCorpResultDTO corpResultDTO = new KOSCorpResultDTO();
        corpResultDTO.setNpc(true);
        corpResultDTO.setType("corp");
        corpResultDTO.setLabel("Pilot's NPC Corp");
        corpResultDTO.setEveId(1000109L);
        pilotResultDTO.setCorp(corpResultDTO);
        containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResultDTO}));

        // Employment History
        CharacterEmployment currentEmployment = new CharacterEmployment();
        ReflectionTestUtils.setField(currentEmployment, "corporationID", 1000109L, Long.class);
        ReflectionTestUtils.setField(currentEmployment, "startDate", new Date(), Date.class);
        CharacterEmployment oldEmployment = new CharacterEmployment();
        ReflectionTestUtils.setField(currentEmployment, "corporationID", 103373655L, Long.class);
        ReflectionTestUtils.setField(currentEmployment, "startDate", new Date(), Date.class);

        // EVE API Pilot Result
        final CharacterInfoResponse eveCharacterResponse = new CharacterInfoResponse();
        eveCharacterResponse.setCorporation("CorpNotFoundByCVA");
        eveCharacterResponse.setAlliance("AllianceFoundByCVA");
        List<CharacterEmployment> employmentHistory = eveCharacterResponse.getEmploymentHistory();
        employmentHistory.add(currentEmployment);
        employmentHistory.add(oldEmployment);

        m_mockery.checking(new Expectations() {{
            // The Pilot found immediately on CVA
            allowing(m_pilotCVACache).get("PilotFoundByCVA");
            will(returnValue(containerResult));
            // Last Corp found
            allowing(m_corpCVACache).get("Random Corp");
            will(throwException(new ExecutionException(new LoadingException("Last Corp lookup went wrong!"))));

            // Allow EVE API lookups
            allowing(m_idToCharacterInfoCache).get(with(any(Long.class)));
            will(returnValue(eveCharacterResponse));
            allowing(m_idToNameCache).get(103373655L);
            will(returnValue("Random Corp"));
            allowing(m_userContext);
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotFoundByCVA");
        assertEquals(EntityStatus.NEUTRAL, result.getStatusSummary().getResolvedStatus());
        assertTrue(result.getStatusSummary().isWarning());
    }

    @Test
    public void shouldNeutralIfNoResultsPilotInNoResultsNonNPCCorp() throws Exception {
        // Setup what CVA returns in this test
        // CVA Pilot Result with 'No Results'
        final KOSResultContainerDTO pilotContainerResult = new KOSResultContainerDTO();
        pilotContainerResult.setTotal(0);
        pilotContainerResult.setCode(100);
        // CVA Corp Result with 'No Results'
        final KOSResultContainerDTO corpContainerResult = new KOSResultContainerDTO();
        corpContainerResult.setTotal(0);
        corpContainerResult.setCode(100);
        // EVE API Pilot Result
        final CharacterInfoResponse eveCharacterResponse = new CharacterInfoResponse();
        eveCharacterResponse.setCharacterName("PilotNoResultsCVAInCorpNoResultsCVA");
        eveCharacterResponse.setCorporation("CorpNotFoundByCVA");
        eveCharacterResponse.setCorporationID(98365915);
        // No alliance

        m_mockery.checking(new Expectations() {{
            // The Pilot not found on CVA
            allowing(m_pilotCVACache).get("PilotNoResultsCVAInCorpNoResultsCVA");
            will(returnValue(pilotContainerResult));
            // The Corp not found on CVA
            allowing(m_corpCVACache).get("CorpNotFoundByCVA");
            will(returnValue(corpContainerResult));

            // Allow EVE API lookups - no assertions for this test
            allowing(m_nameToIdCache).get(with(any(String.class)));
            will(returnValue(0L));
            allowing(m_idToCharacterInfoCache).get(with(any(Long.class)));
            will(returnValue(eveCharacterResponse));

            // No assertions for this test
            allowing(m_userContext);
        }});
        KOSResult result = m_kosChecker.checkPilot("PilotNoResultsCVAInCorpNoResultsCVA");
        assertEquals(EntityStatus.NEUTRAL, result.getStatusSummary().getResolvedStatus());
    }
}